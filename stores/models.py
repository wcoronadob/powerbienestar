from django.db import models


class Store(models.Model):
    """
    Modelo que representa las tiendas que participan    
    en las encuestas.
    """
    name = models.CharField(max_length=255)
    date_modified = models.DateTimeField(auto_now=True)
    date_published = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return self.name