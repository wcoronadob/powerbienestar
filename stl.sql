# Conseguir questionario por questionario id y nombre Secuencia se usa para validar si existe o no
SELECT "survey_question"."id", 
"survey_question"."sequence", 
"survey_question"."questionnaire_id", 
"survey_question"."question", 
"survey_question"."date_modified", 
"survey_question"."date_published" 
FROM "survey_question" 
WHERE ("survey_question"."questionnaire_id" = 1 AND "survey_question"."sequence" = 'P1');


# Crear questionario cuando no existe
INSERT INTO "survey_questionnaire" 
("questionnaire", "date_modified", "date_published") 
VALUES (\'test\', \'2023-03-23T17:37:40.066139+00:00\'::timestamptz, \'2023-03-23T17:37:40.066708+00:00\'::timestamptz);


# Crear Pregunta cuando no existe
INSERT INTO "survey_question" 
("sequence", "questionnaire_id", "question", "date_modified", "date_published") 
VALUES (\'P60\', 1, \'Not defined\', \'2023-03-23T17:56:00.268613+00:00\'::timestamptz, \'2023-03-23T17:56:00.268654+00:00\'::timestamptz);


# Conseguir el Participante por nombre para validar si existe
SELECT "participant_participant"."id", 
"participant_participant"."participant", 
"participant_participant"."email", 
"participant_participant"."date_modified", 
"participant_participant"."date_published" 
FROM "participant_participant" 
WHERE "participant_participant"."participant" = 'ENCUESTADO 1';


# Crear Participante si es que no se encuentra ninguno.
INSERT INTO "participant_participant" 
("participant", "email", "date_modified", "date_published") 
VALUES (\'test\', NULL, \'2023-03-23T20:20:28.986243+00:00\'::timestamptz, \'2023-03-23T20:20:28.986972+00:00\'::timestamptz);


# Buscar Tienda registrada
SELECT "stores_store"."id", 
"stores_store"."name", 
"stores_store"."date_modified", 
"stores_store"."date_published" 
FROM "stores_store" WHERE "stores_store"."id" = 1;


# Crear Tienda en caso de que no este creada
INSERT INTO "stores_store" 
("name", "date_modified", "date_published") 
VALUES (\'test\', \'2023-03-23T20:35:04.454910+00:00\'::timestamptz, \'2023-03-23T20:35:04.455069+00:00\'::timestamptz);


# Flitrar planes de ventaspor mes y por nombre de tienda
SELECT "plans_plan"."id", 
"plans_plan"."store_id", 
"plans_plan"."is_close", 
"plans_plan"."month", 
"plans_plan"."month_date", 
"plans_plan"."amount_plan", 
"plans_plan"."amount_achive", 
"plans_plan"."amount_last_year", 
"plans_plan"."date_modified", 
"plans_plan"."date_published" FROM "plans_plan" 
INNER JOIN "stores_store" ON ("plans_plan"."store_id" = "stores_store"."id") 
WHERE "stores_store"."name" = 'IKF-C80-JAEN 8' and "plans_plan"."month_date"  = '2022-12-31 19:00:00.000 -0500';


# Crear plan en caso de que no hubiera
INSERT INTO "plans_plan" 
("store_id", "is_close", "month", "month_date", "amount_plan", "amount_achive", "amount_last_year", "date_modified", "date_published") 
VALUES (1, false, 1, \'2023-03-23T20:35:04.454910+00:00\'::timestamptz, 1000, 1500, 1200, \'2023-03-23T20:54:29.583079+00:00\'::timestamptz, \'2023-03-23T20:54:29.583092+00:00\'::timestamptz);


# Buscar Lotes de respuestas por ID 
SELECT "survey_batch"."id", 
"survey_batch"."file_name", 
"survey_batch"."date_modified", 
"survey_batch"."date_published", 
"survey_batch"."month", 
"survey_batch"."month_date", 
"survey_batch"."count" 
FROM "survey_batch" WHERE "survey_batch"."id" = 3;


# Crear Lotes de respuestas
INSERT INTO "survey_batch" 
("file_name", "date_modified", "date_published", "month", "month_date", "count") 
VALUES (\'testin.xml\', \'2023-03-23T21:13:28.272306+00:00\'::timestamptz, \'2023-03-23T21:13:28.272328+00:00\'::timestamptz, NULL, NULL, 0);


# Creacion de respuestas para las encuestas.
INSERT INTO "survey_answer" 
("participant_id", "question_id", "batch_id", "answer", "month", "month_date", "date_modified", "date_published", "store_id", "questionnaire_id") 
VALUES (1, 1, 3, 5, 1, \'2023-03-23T20:35:04.454910+00:00\'::timestamptz, \'2023-03-23T21:09:27.749004+00:00\'::timestamptz, \'2023-03-23T21:09:27.749045+00:00\'::timestamptz, 1, 1);
