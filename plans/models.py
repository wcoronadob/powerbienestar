from datetime import datetime, timedelta
from django.db import models
from stores.models import Store
from django.utils.dates import MONTHS


# Create your models here.
class Plan(models.Model):
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    is_close = models.BooleanField(default=False)
    month = models.PositiveBigIntegerField(choices=MONTHS.items())
    month_date = models.DateTimeField(blank=True, null=True)
    amount_plan = models.DecimalField(max_digits=12, decimal_places=2, default=0.0)
    amount_achive = models.DecimalField(max_digits=12, decimal_places=2, default=0.0)
    amount_last_year = models.DecimalField(max_digits=12, decimal_places=2, default=0.0)
    date_modified = models.DateTimeField(auto_now=True)
    date_published = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return self.store.name
