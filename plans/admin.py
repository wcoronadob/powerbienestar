from django.contrib import admin
from plans.models import Plan



@admin.register(Plan)
class PlanAdmin(admin.ModelAdmin):
    list_display = ('store', 'month_date', 'month', 'date_published', 'is_close')
    list_filter = ('month',)
