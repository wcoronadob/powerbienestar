from django.db import models
from participant.models import Participant
from stores.models import Store
from django.utils.dates import MONTHS


class Batch(models.Model):
    file_name = models.CharField(max_length=700)
    date_modified = models.DateTimeField(auto_now=True)
    date_published = models.DateTimeField(auto_now_add=True)
    month = models.PositiveBigIntegerField(choices=MONTHS.items(), blank=True, null=True)
    month_date = models.DateTimeField(blank=True, null=True)
    count = models.IntegerField(default=0)

    def __str__(self) -> str:
        return self.date_published.strftime("%m/%d/%Y, %H:%M:%S")


class Questionnaire(models.Model):
    """
    Clase Cuestionario
    """
    questionnaire = models.TextField()
    date_modified = models.DateTimeField(auto_now=True)
    date_published = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return self.questionnaire


class Question(models.Model):
    """
    Clase Pregunta
    """
    sequence = models.CharField(max_length=10, blank=True, null=True)
    questionnaire = models.ForeignKey(Questionnaire, on_delete=models.CASCADE)
    question = models.TextField()
    date_modified = models.DateTimeField(auto_now=True)
    date_published = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return self.question


class Answer(models.Model):
    """
    Clase Respuesta
    """
    participant = models.ForeignKey(Participant, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    batch = models.ForeignKey(Batch, on_delete=models.CASCADE, blank=True, null=True)
    answer = models.PositiveIntegerField(default=0)
    month = models.PositiveBigIntegerField(choices=MONTHS.items())
    month_date = models.DateTimeField(blank=True, null=True)
    date_modified = models.DateTimeField(auto_now=True)
    date_published = models.DateTimeField(auto_now_add=True)
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    questionnaire = models.ForeignKey(Questionnaire, on_delete=models.CASCADE)
    
    

    def __str__(self) -> str:
        return str(self.answer)