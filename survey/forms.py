from django import forms
from stores.models import Store


class UploadDataForm(forms.Form):
    file = forms.FileField()
    store_id = forms.ModelChoiceField(
        label="Tienda de Origen",
        queryset=Store.objects.all(),
        required=True,
        initial=0
    )
    month_of_origin = forms.TypedChoiceField(
        label="Mes de origen",
        initial='0',
        choices = (
            (0, "-- Seleccionar --"),
            (1, "Enero"),
            (2, "Febrero"),
            (3, "Marzo"),
            (4, "Abril"),
            (5, "Mayo"),
            (6, "Junio"),
            (7, "Julio"),
            (8, "Agosto"),
            (9, "Septiembre"),
            (10, "Octubre"),
            (11, "Noviembre"),
            (12, "Diciembre")
        )
    )


class UploadPlansForm(forms.Form):
    file = forms.FileField()