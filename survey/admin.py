from django.contrib import admin
from survey.models import Question, Questionnaire, Batch, Answer


@admin.register(Batch)
class BatchAdmin(admin.ModelAdmin):
    list_display = ('date_published', 'file_name', 'count')


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('sequence', 'question', 'questionnaire')
    list_filter = ('questionnaire',)


@admin.register(Questionnaire)
class Questionnaire(admin.ModelAdmin):
    pass


@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = ('participant', 'month', 'questionnaire', 'question', 'answer', 'batch', 'store')
    list_filter = ('batch', 'month','store', 'questionnaire',)