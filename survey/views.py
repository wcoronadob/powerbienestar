from datetime import datetime, timedelta, date
from openpyxl import Workbook, load_workbook
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib import messages
from survey.forms import UploadDataForm, UploadPlansForm
from survey.models import Questionnaire, Question, Answer, Batch
from plans.models import Plan
from stores.models import Store
from participant.models import Participant


def upload_data_to_db_from_excel(f, store_id, month_id):
    """
    Basicamente abrimos el excel y agregamos masivamente los datos =) y hacemos alguna que otra validacion
    """
    try:

        current_year = date.today().year
        store_obj = Store.objects.get(pk=store_id)
        time_to_create_plane = None
        month = None

        match month_id:
            case "1":
                time_to_create_plane = datetime(current_year, 1, 1)
                month = 1
            case "2":
                time_to_create_plane = datetime(current_year, 2, 1)
                month = 2
            case "3":
                time_to_create_plane = datetime(current_year, 3, 1)
                month = 3
            case "4":
                time_to_create_plane = datetime(current_year, 4, 1)
                month = 4
            case "5":
                time_to_create_plane = datetime(current_year, 5, 1)
                month = 5
            case "6":
                time_to_create_plane = datetime(current_year, 6, 1)
                month = 6
            case "7":
                time_to_create_plane = datetime(current_year, 7, 1)
                month = 7
            case "8":
                time_to_create_plane = datetime(current_year, 8, 1)
                month = 8
            case "9":
                time_to_create_plane = datetime(current_year, 9, 1)
                month = 9
            case "10":
                time_to_create_plane = datetime(current_year, 10, 1)
                month = 10
            case "11":
                time_to_create_plane = datetime(current_year, 11, 1)
                month = 11
            case "12":
                time_to_create_plane = datetime(current_year, 12, 1)
                month = 12
            case _:
                print("Fecha no reconocida.")

        
        if not store_obj:
            raise Exception('store not found')

        wb_obj = load_workbook(f)
        sheet_obj = wb_obj.active

        if wb_obj:
            batch_obj = Batch(file_name=f)
            batch_obj.save()

        batch_count_anwsers = 0


        for sheet in wb_obj.get_sheet_names(): 
            # Con esto recorremos las hojas que hay en el archivo

            try: 
                questionary_obj = Questionnaire.objects.get(questionnaire__exact=sheet)
            except Questionnaire.DoesNotExist:
                questionary_obj = Questionnaire(
                    questionnaire=sheet
                )
                questionary_obj.save()        

            # questionary_obj = Questionnaire.objects.filter(questionnaire__exact=sheet)

            # if len(questionary_obj) == 0:
            #    raise Exception("Cuestionario no encontrado")

            sp = wb_obj[sheet]
            first_row = sp[1]
            # print(sp.max_column)    

            # Check if there any questions exist
            for fr in first_row[1:]:
                qs_list = Question.objects.filter(sequence=fr.value, questionnaire=questionary_obj.id)
                
                print(len(qs_list))

                for i in qs_list:
                    print(i)
                
                if len(qs_list) == 0:
                    q = Question(
                        sequence=fr.value,
                        questionnaire=questionary_obj,
                        question='not yet defined'
                    )
                    q.save()
                else:
                    print('question alredy exists... i hope soooo!')


            # recorremos los rows y creamos los participantes
            for row in sp.iter_rows(min_row=1, values_only=True):
                participant_list = Participant.objects.filter(participant=row[0])

                print(time_to_create_plane)

                # si no existe el participante lo creamos
                if len(participant_list) == 0 and row[0] != None:
                    par_obj = Participant(
                        participant=str(row[0])
                    )
                    par_obj.save()
                else:
                    print('the participan alredy exist')

                for cell in row[1:]:
                    if type(cell) is not str:
                        # print(type(cell))        
                        anw = 0 if cell == None else cell
                        a = Answer(
                            participant=participant_list.first(),
                            question=qs_list[0],
                            month=month,
                            month_date=time_to_create_plane,
                            batch=batch_obj,
                            answer=int(anw),
                            store=store_obj,
                            questionnaire=questionary_obj
                        )
                        batch_count_anwsers+=1
                        a.save()

        batch_obj.count=batch_count_anwsers
        batch_obj.month = month
        batch_obj.month_date = time_to_create_plane
        batch_obj.save()
        return batch_count_anwsers

    except Exception as err:
       print(f"Unexpected {err=}, {type(err)=}")
       raise 


def upload_plans_to_db(f):
    """
    Lo mismo de arriba pero para los planes
    """
    try:
        wb_obj = load_workbook(f)
        sheet_obj = wb_obj.active

        plans_counter = 0

        current_year = date.today().year

        month_plan = str(sheet_obj["A12"].value).split()[-1]

        cells = sheet_obj['B3':'S9']

        if sheet_obj.max_row != 12:
             raise Exception("Revisa el excel si esta correctamente formateado")

        time_to_create_plane = None

        month = None

        match month_plan:
            case "Enero":
                time_to_create_plane = datetime(current_year, 1, 1)
                month = 1
            case "Febrero":
                time_to_create_plane = datetime(current_year, 2, 1)
                month = 2
            case "Marzo":
                time_to_create_plane = datetime(current_year, 3, 1)
                month = 3
            case "Abril":
                time_to_create_plane = datetime(current_year, 4, 1)
                month = 4
            case "Mayo":
                time_to_create_plane = datetime(current_year, 5, 1)
                month = 5
            case "Junio":
                time_to_create_plane = datetime(current_year, 6, 1)
                month = 6
            case "Julio":
                time_to_create_plane = datetime(current_year, 7, 1)
                month = 7
            case "Agosto":
                time_to_create_plane = datetime(current_year, 8, 1)
                month = 8
            case "Septiembre":
                time_to_create_plane = datetime(current_year, 9, 1)
                month = 9
            case "Octubre":
                time_to_create_plane = datetime(current_year, 10, 1)
                month = 10
            case "Noviembre":
                time_to_create_plane = datetime(current_year, 11, 1)
                month = 11
            case "Diciembre":
                time_to_create_plane = datetime(current_year, 12, 1)
                month = 12
            case _:
                raise Exception("Fecha no renconsida")

        for row in cells:

            # print(row[0].value)
            # print(row[2].value)
            # print(row[4].value)
            # print('--------------------------------')

            plan_obj = Plan.objects.filter(store__name=row[0].value, month_date=time_to_create_plane)

            try:
                store_obj = Store.objects.get(name=row[0].value)
            except Store.DoesNotExist:
                store_obj = Store(name=row[0].value)
                store_obj.save()

            if len(plan_obj) == 0:
                new_plan = Plan(
                    store = store_obj,
                    month = month,
                    month_date = time_to_create_plane,
                    amount_plan =  0 if row[2].value == None else round(row[2].value, 2),
                    amount_achive = 0 if row[4].value == None else round(row[4].value, 2),
                    amount_last_year = 0 if row[1].value == None else round(row[1].value, 2)
                )
                new_plan.save()
                plans_counter+=1

        return plans_counter


    except Exception as err:
        print(f"Unexpected {err=}, {type(err)=}")
        raise 


def upload_data_from_excel(request):
    if request.method == 'POST':
        form = UploadDataForm(request.POST, request.FILES)

        if form.is_valid():
            count = upload_data_to_db_from_excel(request.FILES['file'], store_id=request.POST["store_id"], month_id=request.POST["month_of_origin"])
            if count is None:
                messages.error(request, 'Algo salio mal con la data de las encuestas.')
            else:
                messages.success(request, 'Se subieron los datos de forma correcta.')
            
            return render(request, 'success.html', {'count': count})
    else:
        form = UploadDataForm()
   
    return render(request, 'upload.html', {'form': form})



def upload_plans_from_excel(request):
    if request.method == 'POST':
        form = UploadPlansForm(request.POST, request.FILES)

        if form.is_valid():
            count = upload_plans_to_db(request.FILES['file'])
            if count is None:
                messages.error(request, 'Algo salio mal con la data de los planes.')
            else:
                messages.success(request, 'Se subieron los datos de forma correcta.')

            return render(request, 'success.html', {'count': count})
    else:
        form = UploadPlansForm()

    return render(request, 'upload_plans.html', {'form': form})


def success_page(request):
    return render(request, 'success.html', {})