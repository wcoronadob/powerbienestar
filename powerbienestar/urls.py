import debug_toolbar
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from survey.views import upload_data_from_excel, success_page, upload_plans_from_excel

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('', upload_data_from_excel, name='upload-data'),
    path('planes/', upload_plans_from_excel, name='upload-plans'),
    path('success/', success_page, name='success'),
    path('__debug__/', include('debug_toolbar.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
