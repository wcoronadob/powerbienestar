from django.db import models


class Participant(models.Model):
    participant = models.CharField(max_length=250)
    email = models.EmailField(null=True, blank=True)
    date_modified = models.DateTimeField(auto_now=True)
    date_published = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return self.participant
