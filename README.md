# PowerBienestar

## Deployment

Para hacer funcionar instalar docker y correr 

```bash
  docker-compose build
  docker-compose up
```

## Base de Datos
### Diagrama de Clases
![plot](my_project_subsystem.png)


### Diagrama ER
![plot](postgres-public-all.png)
