# base image  
FROM python:3.10-slim-buster
# setup environment variable  
ENV DockerHOME=/home/app/webapp  

# Install requiments for django-extensions
RUN apt-get update && apt-get install -y graphviz

# set work directory  
RUN mkdir -p $DockerHOME  

# where your code lives  
WORKDIR $DockerHOME  

# set environment variables  
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1  

# install dependencies  
RUN pip install --upgrade pip
RUN pip uninstall django

# copy whole project to your docker home directory. 
COPY requirements.txt $DockerHOME  
# run this command to install all dependencies  
RUN pip install -r requirements.txt

